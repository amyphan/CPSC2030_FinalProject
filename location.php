<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <title>Project</title>
</head>
<body>
	<div class="grid-container">
       

        <div class="item1">
			<div class="logoImg"><img src="images/K.png" height="120" width="120"></div>
            <h2>Kiyomizu-dera Temple</h2>
            <h3>Kyoto, Japan</h3>
        </div>


        <div class="item2">
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>               
        </div>


        <div class="item3">
            <a href="index.php">Home</a>
            <a href="history.php">History</a>
            <a href="festivals.php">Festivals</a> 
            <a href="worship.php">Worship</a>
            <a href="location.php">Location</a>                 
            <a href="shop.php">Shop</a>         
        </div>
        
        <div class="item4">
            <h2>Address</h2>
            <p>1-294, Kiyomizu, Higashiyama-ku, Kyoto-shi, Kyoto, 605-0862, Japan</p><br>

            <h2>Hours</h2>
    		<div id="result"></div>


        </div>


    </div>

	<div class="footer">
        <p>&copy; Kiyomizu-dera Temple</p>
    </div>
</body>
</html>

<script>
	//using ajax and jQuery to pull the datas from the SQL back end
	$('#result').html('');
	$.ajax({
		url:"fetch.php",
		method:"post",
		data:{},
		dataType:"text",
		success:function(data)
		{
			$('#result').html(data);
		} 
	});

</script>