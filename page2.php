<?php

require_once 'sqlhelper.php';
require_once './vendor/autoload.php';  //include the twig library.
session_start();
$twig = setupMyTwigEnvironment(); //moved twig setup code to it's own function, makes code more readable
$conn = connectToMyDatabase();

$item = $_GET["item"];
$query = "SELECT * FROM store WHERE itemName = '$item'";

$result = $conn->query($query);

if($result){
    $details = $result->fetch_all(MYSQLI_ASSOC); 
    $template = $twig->load('detailspage.twig.html');
    echo $template->render(array('details'=>$details,
								'title' => 'OVERVIEW',));


} else {
    dumpErrorPage($twig);
}



?>