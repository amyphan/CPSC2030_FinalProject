<?php

require_once 'sqlhelper.php';
require_once './vendor/autoload.php';  //include the twig library.

$twig = setupMyTwigEnvironment(); //moved twig setup code to it's own function, makes code more readable
$conn = connectToMyDatabase();

$template = $twig->load('historyPage.twig.html');

//display into the webpage
echo $template->render(array('logo'=>'Kiyomizu-dera Temple',
    							'logo2' =>'Kyoto, Japan',
    							'history' => 'THE HISTORY OF KIYOMIZU-DERA TEMPLE',
    							'footer' => 'Kiyomizu-dera Temple',
								));

?>