<?php


function clearConnection($mysql){
    while($mysql->more_results()){
       $mysql->next_result();
       $mysql->use_result();
    }
}
//helper function
function wrap($tag,$value) {
    return "<$tag>$value</$tag>";
}

function connectToMyDatabase(){
    $user = 'ProjectCPSC2030';
    $pwd = 'CPSC2030';
    $server = 'localhost';
    $dbname = 'souvenirs';
 
    $conn = new mysqli($server, $user, $pwd, $dbname);
    return $conn;
}
//twig setup for this web-app
function setupMyTwigEnvironment(){
    $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
    $twig = new Twig_Environment($loader); 
    return $twig;  
}

function dumpErrorPage($twig){
    $template = $twig->load("error.twig.html");
    echo $template->render(array("message"=>"SQL errorm query failed"));
}
?>

