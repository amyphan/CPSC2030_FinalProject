<?php

   require_once 'sqlhelper.php';
   require_once './vendor/autoload.php';  //include the twig library.
   session_start();

   $loader = new Twig_Loader_Filesystem('./templates'); 
    //Sql setup
   $twig = new Twig_Environment($loader);
   $conn = connectToMyDatabase();
   $query = "SELECT itemName, ID, price, color, stockRemain FROM store";
   $result = $conn->query($query);
   

   if($result){
      $table = $result->fetch_all(MYSQLI_ASSOC);
      $template = $twig->load('shopPage.twig.html');

      //display into the webpage
      echo $template->render(array('logo' => 'Kiyomizu-dera Temple',
                                  'logo2' =>'Kyoto, Japan',
                                  'store' => $table,
                                  'footer' => 'Kiyomizu-dera Temple',
                 
        ));
    
      $conn->close(); 
   }else {
    $template = $twig->load("error.twig.html");
    echo $template->render(array("message"=>"SQL errorm query failed!!"));
   }
?>