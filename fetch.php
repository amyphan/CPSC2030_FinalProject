<?php
require_once 'sqlhelper.php';

$conn = connectToMyDatabase();

$output = '';
$query = "SELECT * FROM hours";
$result = $conn->query($query);

//pulling data from a SQL back end using AJAX and jQuery
//implement a non-trivial feature using jQuery
if ($result->num_rows > 0) {
    $output .= '<div class="table-responsive">
    				<table border="1">

    					<tr>
    						<th>Period</th>
    						<th>Opened at</th>
    						<th>Closed at</th>
    					</tr>';
    while($row = $result->fetch_assoc()) {
    	$output .= '
    		<tr>
    			<td>'.$row["period"].'</td>
    			<td>'.$row["open"].'</td>
    			<td>'.$row["close"].'</td>
    		</tr>';
                       
    }
     echo $output;     
                    
} 
else {
    echo "0 results";
}


?>