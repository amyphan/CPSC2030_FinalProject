-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2018 at 03:40 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `souvenirs`
--

-- --------------------------------------------------------

--
-- Table structure for table `hours`
--

CREATE TABLE `hours` (
  `period` text NOT NULL,
  `open` text NOT NULL,
  `close` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hours`
--

INSERT INTO `hours` (`period`, `open`, `close`) VALUES
('Jan 1 - Mar 8', '6:00 AM', '6:00 PM'),
('Mar 9 - Mar 18 (Special night viewing in Spring)', '6:00 AM', '9:00 PM'),
('Mar 19 - Mar 29', '6:00 AM', '6:00 PM'),
('Mar 30 - Apr 8 (Special night viewing in Spring)', '6:00 AM', '9:00 PM'),
('Apr 9 - Jun 30', '6:00 AM', '6:00 PM'),
('Jul 1 - Aug 13', '6:00 AM', '6:30 PM'),
('Aug 14 - Aug 16 (Special night viewing in Summer)', '6:00 AM', '9:00 PM'),
('Aug 17 - Aug 31', '6:00 AM', '6:30 PM'),
('Sep 1 - Nov 16', '6:00 AM', '6:00 PM'),
('Nov 17 - Dec 2 (Special night viewing in Autumn)', '6:00 AM', '9:00 PM'),
('Dec 2 - Dec 31', '6:00 AM', '6:00 PM');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
