-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2018 at 04:14 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `souvenirs`
--

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `itemName` text NOT NULL,
  `ID` int(11) NOT NULL,
  `price` text NOT NULL,
  `color` text NOT NULL,
  `stockRemain` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`itemName`, `ID`, `price`, `color`, `stockRemain`, `description`) VALUES
('Buddha statue 1', 10001, '$27.99', 'Gold', 'Only a few left', 'This beautiful Buddha statue will bring a calming influence into any room\r\nSpeckled bronze finish appears metallic\r\n9.5 x 6.25 x 11.8 inches tall'),
('Buddha Statue 2', 10002, '$27.99', 'White', 'In Stock', 'This beautiful Buddha statue will bring a calming influence into any room\r\nSpeckled bronze finish appears metallic\r\n9.5 x 6.25 x 11.8 inches tall'),
('Buddha Statue 3', 10003, '$27.99', 'Gray', 'In Stock', 'This beautiful Buddha statue will bring a calming influence into any room\r\nSpeckled bronze finish appears metallic\r\n9.5 x 6.25 x 11.8 inches tall'),
('Buddha Statue 4', 10004, '$38.99', 'White', 'In Stock', 'Pearl Moonlight Meditating Buddha Seated Religious Thought Praying Syle\r\nThis product is made of non-toxic materials and is safe for the environment.'),
('Buddha Statue 5', 10005, '$38.99', 'Silver', 'Out Of Stock', 'Pearl Moonlight Meditating Buddha Seated Religious Thought Praying Syle\r\nThis product is made of non-toxic materials and is safe for the environment.'),
('Buddha Statue 6', 10006, '$38.99', 'Green', 'In Stock', 'Pearl Moonlight Meditating Buddha Seated Religious Thought Praying Syle\r\nThis product is made of non-toxic materials and is safe for the environment.'),
('Happy Buddha Statue 1', 10007, '$99.99', 'Gray', 'In Stock', '166 inch - Luck Housewarming gift\r\nMaterial: Fibreclay. 18x16x16.5\r\n'),
('Happy Buddha Statue 2', 10008, '$99.99', 'White', 'Only a few left', '166 inch - Luck Housewarming gift\r\nMaterial: Fibreclay. 18x16x16.5\r\n'),
('Happy Buddha Statue 3', 10009, '$99.99', 'Gold', 'In Stock', '166 inch - Luck Housewarming gift\r\nMaterial: Fibreclay. 18x16x16.5\r\n'),
('Mini Statue 1', 10010, '$8.99', 'Yellow', 'Out Of Stock', 'Material: porcelain\r\nMini size,easy to carry. 4.5x4.5x9.5cm'),
('Mini Statue 2', 10011, '$8.99', 'Pink', 'In Stock', 'Material: porcelain\r\nMini size,easy to carry. 4.5x4.5x9.5cm'),
('Mini Statue 3', 10012, '$8.99', 'Green', 'In Stock', 'Material: porcelain\r\nMini size,easy to carry. 4.5x4.5x9.5cm'),
('Buddha Set 1', 10013, '$28.99', 'Blue', 'In Stock', 'Set mini Buddha monk believed for cure good luck, abundance, happiness, success and good health'),
('Buddha Set 2', 10014, '$28.99', 'Green', 'Only a few left', 'Set mini Buddha monk believed for cure good luck, abundance, happiness, success and good health'),
('Buddha Set 3', 10015, '$28.99', 'White', 'Out Of Stock', 'Set mini Buddha monk believed for cure good luck, abundance, happiness, success and good health'),
('Buddha Incense Burner 1', 10016, '$35.99', 'Orange', 'In Stock', 'It\'s helpful in calming the mind and helping to ease the user into a meditative state.'),
('Buddha Incense Burner 1', 10017, '$35.99', 'Silver', 'In Stock', 'It\'s helpful in calming the mind and helping to ease the user into a meditative state.'),
('Buddha Incense Burner 3', 10018, '$35.99', 'Pink', 'In Stock', 'It\'s helpful in calming the mind and helping to ease the user into a meditative state.');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
